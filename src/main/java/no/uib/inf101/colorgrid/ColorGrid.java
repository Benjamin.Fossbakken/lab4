package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.List;
import java.util.ArrayList;

public class ColorGrid implements IColorGrid {

  private List<List<Color>> grid;

  public ColorGrid(int rows, int cols) {
    grid = new ArrayList<>();

    for (int i = 0; i < rows; i++) {
      grid.add(new ArrayList<>());
      for (int j = 0; j < cols; j++) {
        grid.get(i).add(null);

      }
    }
  }

  @Override
  public int rows() {
    return grid.size();
  }

  @Override
  public int cols() {
    return grid.get(0).size();
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cells = new ArrayList<CellColor>();

    for (int i = 0; i < rows(); i++) {
      for (int j = 0; j < cols(); j++) {
        CellPosition pos = new CellPosition(i, j);
        cells.add(new CellColor(pos, get(pos)));
      }
    }
    return cells;
  }

  @Override
  public Color get(CellPosition pos) {
    return grid.get(pos.row()).get(pos.col());
  }

  @Override
  public void set(CellPosition pos, Color color) {
    grid.get(pos.row()).set(pos.col(), color);
    
  }

}

