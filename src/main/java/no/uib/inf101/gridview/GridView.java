package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;


public class GridView extends JPanel {

  IColorGrid grid;

  private static final int OUTER_MARGIN = 30;

  private static final Color BACKGROUND_COLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid grid) {
    this.grid = grid;
    this.setPreferredSize(new Dimension(400, 300));
  }


  @Override
  public void paintComponent(Graphics g) {
    Graphics2D g2 = (Graphics2D) g;
    super.paintComponent(g2);

    drawGrid(g2);
  
  }

  void drawGrid(Graphics2D g2) {
    Rectangle2D box = new Rectangle2D.Double(OUTER_MARGIN, OUTER_MARGIN, getWidth()-OUTER_MARGIN*2, getHeight()-OUTER_MARGIN*2);
    g2.setColor(BACKGROUND_COLOR);
    g2.fill(box);

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(box, grid, OUTER_MARGIN);

    drawCells(g2, grid, converter);

  }
  static void drawCells(Graphics2D g2, CellColorCollection cells, CellPositionToPixelConverter converter){
    for (CellColor cp : cells.getCells()) {
      Rectangle2D bounds = converter.getBoundsForCell(cp.cellPosition());
      Color color = cp.color();
      g2.setColor(color);
      if (color == null) {
        g2.setColor(Color.DARK_GRAY);
      }
      g2.fill(bounds);
    }

  }


}
